import './App.css';
import {useState, useEffect} from "react";
import Board from "./components/Board";
import Square from "./components/Square";

const defaultSquares = () => (new Array(9)).fill(null);

const lines = [
    [0,1,2], [3,4,5], [6,7,8],
    [0,3,6], [1,4,7], [2,5,8],
    [0,4,8], [2,4,6]
];

function App() {
    const [squares, setSquares] = useState(defaultSquares());
    const [winner, setWinner] = useState(null);

    useEffect(() =>{
        const isComputerTurn = squares.filter(square => square !== null).length % 2 === 1;
        const linesThatAre = (a,b,c) => {
            return lines.filter(squareIndexes => {
                const squareValues = squareIndexes.map(index => squares[index]);
                return JSON.stringify([a,b,c].sort()) === JSON.stringify(squareValues.sort());
            });
        };
        const emptyIndex = squares
            .map((square, index) => square === null ? index : null)
            .filter(val => val != null);

        const playerWon = linesThatAre('x', 'x', 'x').length > 0;
        const computerWon = linesThatAre('o', 'o', 'o').length > 0;
        if (playerWon){
            setWinner('x');
        }
        if (computerWon){
            setWinner('o');
        }

        const putComputerAt = index => {
            let newSquars = squares
            newSquars[index] = 'o';
            setSquares([...newSquars]);
        };
        if (isComputerTurn){

            const winningLines = linesThatAre('o', 'o', null);
            if (winningLines.length > 0){
                const winIndex = winningLines[0].filter(index => squares[index] === null)[0];
                putComputerAt(winIndex);
                return;
            }

            const linesToBlock = linesThatAre('x', 'x', null);
            if (linesToBlock.length > 0) {
                const blockIndex = linesToBlock[0].filter(index => squares[index] === null)[0];
                putComputerAt(blockIndex);
                return;
            }

            const linesToContinue = linesThatAre('o', null, null);
            if (linesToContinue.length > 0){
                putComputerAt(linesToContinue[0].filter(index => squares[index] === null)[0])
                return;
            }

            const randomIndex = emptyIndex[Math.ceil(Math.random()*emptyIndex.length)];
            putComputerAt(randomIndex);
        }
    }, [squares]);

    function handelSquareClick(index){
        const isPlayerTurn = squares.filter(square => square!==null).length % 2 === 0;
        if(isPlayerTurn){
            let newSquares = squares;
            newSquares[index] = 'x';
            setSquares([...newSquares]);
        }
    }

  return (
    <main>
      <Board>
          {squares.map((square, index) =>
              <Square
                  x={square === 'x' ? 1 : 0}
                  o={square === 'o' ? 1 : 0}
                  onClick={() => handelSquareClick(index)} />
          )}
      </Board>
        {!!winner && winner === 'x' && (
            <div className="result green">
                YOU WON!
            </div>
        )}

        {!!winner && winner === 'o' && (
            <div className="result red">
                YOU LOST!
            </div>
        )}
    </main>
  );
}

export default App;
